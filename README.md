# Simplehtmldom API

The module is a bridge between PHP Simple HTML DOM Parser (simplehtmldom) library and Drupal.
The library provides powerful API for HTML parsing. Moreover, it works fine with broken markup.
After installing this module you will be able to use PHP Simple HTML DOM Parser functions right in your code.
See more details about library usage here http://simplehtmldom.sourceforge.net/

For a full description of the module, visit the
[project page](https://www.drupal.org/project/simplehtmldom).

To submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/simplehtmldom).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

The module requires PHP Simple HTML DOM Parser library.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

No configuration is needed.


## Maintainers

- rsvelko - [rsvelko](https://www.drupal.org/u/rsvelko)
- Konstantin Komelin - [konstantin-komelin](https://www.drupal.org/u/konstantin-komelin)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
