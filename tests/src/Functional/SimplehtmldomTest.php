<?php

namespace Drupal\Tests\simplehtmldom\Functional;

use Drupal\Tests\BrowserTestBase;
use simplehtmldom\HtmlWeb;

/**
 * Class SimplehtmldomTest. Provide default tests for simplehtmldom module.
 */
class SimplehtmldomTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['simplehtmldom'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test HTMLWeb class.
   */
  public function testBodyClasses() {
    $doc = new HtmlWeb();
    $html = $doc->load('https://www.google.com/');
    $headers = $html->find('title');

    $this->assertTrue(in_array('Google', $headers[0]->_));
  }

}
